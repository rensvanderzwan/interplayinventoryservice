﻿using System;
using System.Collections.Generic;
using System.Text;

namespace InterplayInventoryService.domain
{
    public class SearchFilter
    {
        public string Name { get; set; }
        public string Group { get; set; }
        public string Value { get; set; }
        public string Condition { get; set; }
    }
}
