﻿using System;
using System.Collections.Generic;
using System.Text;

namespace InterplayInventoryService.domain
{
    public class AssetCollection
    {
        public List<MasterClip> masterclips { get; set; }
        public List<Sequence> sequences { get; set; }
        public List<Relative> relatives { get; set; }
    }
}
