﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.Text;

namespace InterplayInventoryService.domain
{
    public class MasterClip
    {
        public string MobID { get; set; }
        public string AssetName { get; set; }
        public string VideoID { get; set; }
        public string InterplayPath { get; set; }
        public string Duration { get; set; }
        public string Tracks { get; set; }
        public long MediaSize { get; set; }
        public string MediaFileFormat { get; set; }
        public string MediaStatus { get; set; }
        public DateTime CreationDate { get; set; }
        public DateTime ModifyDate { get; set; }
        public string Workspace { get; set; }
        public bool InUse { get; set; }
        public string Type { get; set; }
    }
}
