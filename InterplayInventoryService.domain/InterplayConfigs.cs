﻿using System;
using System.Collections.Generic;
using System.Text;

namespace InterplayInventoryService.domain
{
    public class InterplayConfigs
    {
        public InterplayConfig InterplayConfigHlv { get; set; }
        public InterplayConfig InterplayConfigDhg { get; set; }
    }

    public class InterplayConfig
    {
        public string AssetEndPoint { get; set; }
        public string TransferEndPoint { get; set; }
        public string WorkgroupUri { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string Location { get; set; }
    }
}
