﻿using System;
using System.Collections.Generic;
using System.Text;

namespace InterplayInventoryService.domain
{
    public class Project
    {
        public string Projectpath { get; set; }
        public string Division { get; set; }
        public string ProjectType { get; set; }
        public long ProjectMasterclipSize { get; set; }
        public long ProjectMasterclipNotInUseSize { get; set; }
        public long ProjectRelativesSize { get; set; }
        public long ProjectHrs { get; set; }
        public DateTime CreationDate { get; set; }
    }
}
