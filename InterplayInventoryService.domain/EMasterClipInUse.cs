﻿using System;
using System.Collections.Generic;
using System.Text;

namespace InterplayInventoryService.domain
{
    public class EMasterClipInUse
    {
        public const bool InUse = true;
        public const bool NotInUse = false;
    }
}
