﻿using System;
using System.Collections.Generic;
using System.Text;

namespace InterplayInventoryService.domain
{
    public class ProjectRelative
    {
        public string AssetName { get; set; }
        public string RelativeName { get; set; }
        public bool IsRelative { get; set; }
        public string SequenceMobID { get; set; }
        public string MasterclipMobID { get; set; }
        public long DurationInFrames { get; set; }
        public long MediasSizeKB { get; set; }
        public string MediasStatus { get; set; }
        public bool InUse { get; set; }
        public string Tracks { get; set; }
        public string Workspace { get; set; }
        public int Ocurrence { get; set; }
        public string[] Sequences { get; set; }
    }
}
