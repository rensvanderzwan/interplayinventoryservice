using InterplayInventoryService.core;
using InterplayInventoryService.domain;
using InterplayInventoryService.Health;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Nos.Health;
using Nos.Logger;
using Nos.Logger.Settings;
using Nos.ServiceBus;
using Nos.ServiceBus.Config;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InterplayInventoryService
{
    public class Startup
    {
        private IConfiguration _configuration { get; }
        public Startup(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
     //       services.AddTransient<InterplayAssetService>();
            services.Configure<InterplayConfigs>(options => _configuration.GetSection("InterplayConfigs").Bind(options));
            var inosConfig = _configuration.GetSection("InterplayConfigs").Get<InterplayConfigs>();

            services.AddNosLogging(_configuration.GetSection("Logging").Get<LogSettings>());

            services.AddNosMessaging(_configuration.GetSection("RabbitMq").Get<RabbitMqConfig>(), bconf => {
                bconf.ExchangeDeclare("e.log", "topic", false);
                bconf.QueueDeclare("q.log", false, false, false, null);
                bconf.QueueBind("q.log", "e.log", "e.log.*");
            });



            services.AddTransient<DatabaseIO>();
            services.AddTransient<Helpers>();

            services.AddNosHealthChecks().AddCheck<RabbitHealthCheck>("Rabbit MQ");
            services.AddHostedService<InventoryHostedService>();

            services.AddRazorPages();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapNosHealthChecks();
                endpoints.MapRazorPages();
            });
        }
    }
}
