﻿using Microsoft.Extensions.Diagnostics.HealthChecks;
using Nos.ServiceBus.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace InterplayInventoryService.Health
{
    public class RabbitHealthCheck : IHealthCheck
    {
        private readonly IBusConnection _connection;
        public RabbitHealthCheck(IBusConnection connection)
        {
            _connection = connection;
        }
        public Task<HealthCheckResult> CheckHealthAsync(HealthCheckContext context,
            CancellationToken cancellationToken = default)
        {
            if (_connection.GetConnection().IsOpen)
                return Task.FromResult(HealthCheckResult.Healthy("RabbitMQ Connection is open"));
            return Task.FromResult(HealthCheckResult.Unhealthy("RabbitMQ Connection is closed"));
        }
    }
}
