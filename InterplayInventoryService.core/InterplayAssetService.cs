﻿using Interplay_hlv_2;
using InterplayInventoryService.domain;
using Newtonsoft.Json;
using Nos.Logger.Contracts;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InterplayInventoryService.core
{
    public class InterplayAssetService
    {
        private readonly INosLogger _logger;
        public string _interplayUri;
        AssetsPortTypeClient _assetsPortTypeClient;
        UserCredentialsType _user;

        public InterplayAssetService(AssetsPortTypeClient assetsPortTypeClient, string name, string password, string InterplayURI, INosLogger Logger)
        {
            _assetsPortTypeClient = assetsPortTypeClient;
            _user = new UserCredentialsType() { Username = name, Password = password };
            _interplayUri = InterplayURI;
            _logger = Logger;
        }

        public async Task<List<AssetDescriptionType>> ExcecuteSearchAsync(string InterplayFolder, SearchGroupType SearchGroup)
        {

     
            AttributeType[] returnAttributes = new AttributeType[] 
            {
                new AttributeType() { Group = "SYSTEM", Name = "Media Size" },
                new AttributeType() { Group = "SYSTEM", Name = "Media Status" },
                new AttributeType() { Group = "SYSTEM", Name = "Modified Date" },
                new AttributeType() { Group = "SYSTEM", Name = "Creation Date" },
                new AttributeType() { Group = "SYSTEM", Name = "Tracks" },
                new AttributeType() { Group = "SYSTEM", Name = "Media Size" },
                new AttributeType() { Group = "SYSTEM", Name = "Media File Format" },
                new AttributeType() { Group = "SYSTEM", Name = "Duration" },
                new AttributeType() { Group = "SYSTEM", Name = "Path" },
                new AttributeType() { Group = "SYSTEM", Name = "Type" },
                new AttributeType() { Group = "USER", Name = "Display Name" },
                new AttributeType() { Group = "USER", Name = "Video ID" },
                new AttributeType() { Group = "USER", Name = "iNOS_Story_ID" },
                new AttributeType() { Group = "SYSTEM", Name = "Workspace" },
                new AttributeType() { Group = "SYSTEM", Name = "Record Complete" },
            };

            SearchType Search = new SearchType()
            {
                InterplayPathURI = $"{_interplayUri}/{InterplayFolder}",
                MaxResults = 200,
                MaxResultsSpecified = false,
                SearchGroup = SearchGroup, 
                ReturnAttributes  = returnAttributes,
            };

            try
            {
                SearchResponse SearchResult = await _assetsPortTypeClient.SearchAsync(_user, Search);
                if (SearchResult.SearchResponse1.Errors == null)
                {
                    return SearchResult.SearchResponse1.Results.ToList();
                }
                else
                {
                    if (SearchResult.SearchResponse1.Errors.Any(x => x.Code == "PATH_NOT_FOUND"))
                    {
                        _logger.Log(Nos.Logger.Models.ELogLevel.Warn, $"ExcecuteSearchAsync: {_assetsPortTypeClient.Endpoint.Address.Uri.AbsoluteUri}", JsonConvert.SerializeObject(SearchResult, Formatting.Indented));
                    }
                    else
                    {
                        _logger.Log(Nos.Logger.Models.ELogLevel.Error, $"ExcecuteSearchAsync: {_assetsPortTypeClient.Endpoint.Address.Uri.AbsoluteUri}", JsonConvert.SerializeObject(SearchResult, Formatting.Indented));
                    }
                    return new List<AssetDescriptionType>() { };
                }
            }
            catch (Exception ex)  //timeout
            {
                _logger.Log(Nos.Logger.Models.ELogLevel.Error, $"ExcecuteSearchAsync:  {_assetsPortTypeClient.Endpoint.Address.Uri.AbsoluteUri} {ex.Message}", JsonConvert.SerializeObject(ex.InnerException, Formatting.Indented));
                return new List<AssetDescriptionType> { };
            }
        }

        public async Task<AssetCollection> GetAllClips(string interplayFolderURI, AssetCollection collection, bool includeReferenceAssets = true)
        {
            GetChildrenType getChildren = new GetChildrenType() { InterplayURI = interplayFolderURI, IncludeFolders = true, IncludeFiles = false, NameFilter = ".*", IncludeRefAssets = includeReferenceAssets, IncludeRefAssetsSpecified = true };
            GetChildrenResponse response = await _assetsPortTypeClient.GetChildrenAsync(_user, getChildren);
            if (response.GetChildrenResponse1.Errors != null)
            {
                ErrorType[] errors = response.GetChildrenResponse1.Errors;
                _logger.Log(Nos.Logger.Models.ELogLevel.Error, "GetAllClips", JsonConvert.SerializeObject(errors));
                return collection;
            }

            AssetDescriptionType[] interplayAsset = response.GetChildrenResponse1.Results;
            AssetDescriptionType[] InterplayFolderAssets = interplayAsset.Where(x => x.InterplayURI.EndsWith("/")).ToArray();
            AssetDescriptionType[] InterplayMasterClips = interplayAsset.Where(x => (x.Attributes.FirstOrDefault(y => y.Name == "Type").Value == "masterclip")).ToArray();
            AssetDescriptionType[] InterplaySequences = interplayAsset.Where(x => x.Attributes.FirstOrDefault(y => y.Name == "Type").Value == "sequence").ToArray();

            foreach (AssetDescriptionType masterClipDescription in InterplayMasterClips)
            {
                MasterClip asset = new MasterClip()
                {
                    MobID = masterClipDescription.Attributes.FirstOrDefault(x => x.Name == "MOB ID").Value,
                    AssetName = masterClipDescription.Attributes.FirstOrDefault(x => x.Name == "Display Name")?.Value ?? masterClipDescription.Attributes.FirstOrDefault(x => x.Name == "MOB ID").Value,
                    InterplayPath = Path.GetDirectoryName(masterClipDescription.Attributes.FirstOrDefault(x => x.Name == "Path").Value).Replace(@"\", "/"),
                    VideoID = masterClipDescription.Attributes.FirstOrDefault(x => x.Name == "Video ID")?.Value,
                    Duration = masterClipDescription.Attributes.FirstOrDefault(x => x.Name == "Duration")?.Value,
                    Tracks = masterClipDescription.Attributes.FirstOrDefault(x => x.Name == "Tracks")?.Value,
                    MediaFileFormat = masterClipDescription.Attributes.FirstOrDefault(x => x.Name == "Media File Format")?.Value,
                    MediaSize = Convert.ToInt64(masterClipDescription.Attributes.FirstOrDefault(x => x.Name == "Media Size")?.Value),
                    MediaStatus = masterClipDescription.Attributes.FirstOrDefault(x => x.Name == "Media Status")?.Value,
                };

                if(asset.MediaStatus != "offline")
                {
                    collection.masterclips.Add(asset);
                }
            }


            foreach (AssetDescriptionType Asset in InterplayFolderAssets)
            {
                collection = await GetAllClips(Asset.InterplayURI, collection);
            }
            _logger.Log(Nos.Logger.Models.ELogLevel.Info, null, $"InterplayFolder: {interplayFolderURI} masterClips:{InterplayMasterClips.Count()} sequences:{InterplaySequences.Count()}");
            return collection;   
        }

        public SearchGroupType SequenceSearchQuery()
        {
            return CreateSearchQuery("AND", new List<SearchFilter>(){
                 new SearchFilter(){ Group = "SYSTEM", Name = "Type", Value = "Sequence", Condition = "EQUALS" },
                 new SearchFilter(){ Group = "SYSTEM", Name = "Duration", Value = "00:00:10:00", Condition = "GREATER_THAN" },
                });
        }

        public SearchGroupType ProjectFoldersSearchQuery()
        {
            return CreateSearchQuery("AND", new List<SearchFilter>(){
                 new SearchFilter(){ Group = "SYSTEM", Name = "Type", Value = "folder", Condition = "EQUALS" },
          //       new SearchFilter(){ Group = "USER", Name = "iNOS_Story_ID", Value = "", Condition = "NOT_EQUALS" },
                });
        }

        public SearchGroupType MasterclipSearchQuery(bool inUse)
        {
            return CreateSearchQuery("AND", new List<SearchFilter>(){
                 new SearchFilter(){ Group = "SYSTEM", Name = "Media Status", Value = "offline", Condition = "NOT_EQUALS" },
                 new SearchFilter(){ Group = "SYSTEM", Name = "Type", Value = "Masterclip", Condition = "EQUALS" },
                 new SearchFilter(){ Group = "SYSTEM", Name = "Tracks", Value = "V", Condition = "CONTAINS" },
                 new SearchFilter(){ Group = "SYSTEM", Name = "Duration", Value = "00:00:10:00", Condition = "GREATER_THAN" },
                 new SearchFilter(){ Group = "SYSTEM", Name = "Workspace", Value = "archief-media", Condition = "NOT_EQUALS" },
                 new SearchFilter(){ Group = "SYSTEM", Name = "Workspace", Value = "bric-media", Condition = "NOT_EQUALS" },
                },
                new FileInUseConditionType() { InUse = inUse });
        }

        public SearchGroupType UpdateSearchQuery(DateTime SearchDateFrom)
        {
            string SearchDate = SearchDateFrom.ToString("yyyy-MM-ddTHH:mm:ss.fffzzz");
            return CreateSearchQuery("AND", new List<SearchFilter>(){
                 new SearchFilter(){ Group = "SYSTEM", Name = "Media Status", Value = "online", Condition = "EQUALS" },
                 new SearchFilter(){ Group = "SYSTEM", Name = "Type", Value = "Masterclip", Condition = "EQUALS" },
                 new SearchFilter(){ Group = "SYSTEM", Name = "Tracks", Value = "V", Condition = "CONTAINS" },
                 new SearchFilter(){ Group = "SYSTEM", Name = "Creation Date", Value = SearchDate, Condition = "GREATER_THAN" },
                });
        }

        public AttributeConditionType CreateConditionType(SearchFilter filter)
        {
            var SearchAttribute1 = new AttributeType() { Group = filter.Group, Name = filter.Name, Value = filter.Value };
            return new AttributeConditionType() { Condition = filter.Condition, Attribute = SearchAttribute1 };
        }

        public SearchGroupType CreateSearchQuery(string Operator, List<SearchFilter> filters, FileInUseConditionType fileInUseCondition = null)
        {
            var attributeConditionTypes = new List<AttributeConditionType>();
            foreach (var filter in filters)
                attributeConditionTypes.Add(CreateConditionType(filter));
            var searchGroup = new SearchGroupType() { Operator = Operator, AttributeCondition = attributeConditionTypes.ToArray(), FileInUseCondition = fileInUseCondition};
            return new SearchGroupType() { Operator = "OR", SearchGroup = new List<SearchGroupType>() { searchGroup }.ToArray() };
        }

        public async Task<AssetDescriptionType[]> FindRelatives(string uri)
        {
            AttributeType[] returnAttributes = new AttributeType[]
{
                new AttributeType() { Group = "SYSTEM", Name = "ReferencedAsset" },
                new AttributeType() { Group = "SYSTEM", Name = "Media Size" },
                new AttributeType() { Group = "SYSTEM", Name = "Media Status" },
                new AttributeType() { Group = "SYSTEM", Name = "Modified Date" },
                new AttributeType() { Group = "SYSTEM", Name = "Creation Date" },
                new AttributeType() { Group = "SYSTEM", Name = "Tracks" },
                new AttributeType() { Group = "SYSTEM", Name = "Media Size" },
                new AttributeType() { Group = "SYSTEM", Name = "Media File Format" },
                new AttributeType() { Group = "SYSTEM", Name = "Duration" },
                new AttributeType() { Group = "SYSTEM", Name = "Path" },
                new AttributeType() { Group = "SYSTEM", Name = "Type" },
                new AttributeType() { Group = "USER", Name = "Display Name" },
                new AttributeType() { Group = "USER", Name = "Video ID" },
                new AttributeType() { Group = "USER", Name = "iNOS_Story_ID" },
                new AttributeType() { Group = "SYSTEM", Name = "Workspace" },
};

            FindRelativesType relatives = new FindRelativesType() { InterplayURI = uri , ReturnAttributes = returnAttributes};
            FindRelativesResponse response = await _assetsPortTypeClient.FindRelativesAsync(_user, relatives);
            if (response.FindRelativesResponse1.Errors != null)
            {
                ErrorType[] errors = response.FindRelativesResponse1.Errors;
                _logger.Log(Nos.Logger.Models.ELogLevel.Error, "FindRelatives", JsonConvert.SerializeObject(errors));
                return new AssetDescriptionType[0];
            }

            return response.FindRelativesResponse1.Results;
        }
    }
}
