﻿using Interplay_hlv_2;
using InterplayInventoryService.domain;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Options;
using Nos.Logger.Contracts;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace InterplayInventoryService.core
{
    public class InventoryHostedService : IHostedService
    {
        private Timer _fullTimer;
        private Timer _updateTimer;
        private int _intervalFullInSeconds = 3600 * 24 * 1;
        private int _intervalUpdateInSeconds = 3600;
        protected InterplayAssetService _hlvInterplayService;
        protected InterplayAssetService _dhgInterplayService;
        private INosLogger _logger;
        private bool _isFullRunning;
        private bool _isUpdateRunning;
        private DatabaseIO _database;


        public InventoryHostedService(IOptions<InterplayConfigs> interplayConfigs, INosLogger logger, DatabaseIO database)
        {
            _logger = logger;
            _database = database;
            string SourceInterplayUri = interplayConfigs.Value.InterplayConfigHlv.WorkgroupUri;
            string SourcetUrl = interplayConfigs.Value.InterplayConfigHlv.AssetEndPoint;
            _hlvInterplayService = new InterplayAssetService(new AssetsPortTypeClient(new BasicHttpBinding() { MaxReceivedMessageSize = 360000000, MaxBufferSize = 360000000, ReceiveTimeout = TimeSpan.FromMinutes(12), SendTimeout = TimeSpan.FromMinutes(12) },
                                                      new EndpointAddress(SourcetUrl)
                                                    ),
                                                    interplayConfigs.Value.InterplayConfigHlv.UserName, interplayConfigs.Value.InterplayConfigHlv.Password, SourceInterplayUri, _logger);

            string TargetInterplayUri = interplayConfigs.Value.InterplayConfigDhg.WorkgroupUri;
            string TargetUrl = interplayConfigs.Value.InterplayConfigDhg.AssetEndPoint;
            _dhgInterplayService = new InterplayAssetService(new AssetsPortTypeClient(new BasicHttpBinding() { MaxReceivedMessageSize = 360000000, MaxBufferSize = 360000000, ReceiveTimeout = TimeSpan.FromMinutes(12), SendTimeout = TimeSpan.FromMinutes(12) },
                                                     new EndpointAddress(TargetUrl)
                                                   ),
                                                   interplayConfigs.Value.InterplayConfigDhg.UserName, interplayConfigs.Value.InterplayConfigDhg.Password, SourceInterplayUri, _logger);
        }


        public Task StartAsync(CancellationToken cancellationToken)
        {
           _fullTimer = new Timer(async (x) => await NeverRunFullConcurrent(), null, TimeSpan.Zero, TimeSpan.FromSeconds(_intervalFullInSeconds));
        //   _updateTimer = new Timer(async (x) => await NeverRunGetProjectsConcurrent(), null, TimeSpan.Zero, TimeSpan.FromSeconds(_intervalUpdateInSeconds));
            return Task.CompletedTask;
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            _fullTimer.Change(Timeout.Infinite, 0);
            return Task.CompletedTask;
        }

        public void Dispose()
        {
            _fullTimer.Dispose();
        }

        public async Task NeverRunFullConcurrent()
        {
            if (_isFullRunning == true)
            {
                _logger.Log(Nos.Logger.Models.ELogLevel.Error, "Previous scheduled job is still running.", null);
            }
            else
            {
                await GetMasterClipsSequencesAndRelatives();
                _isFullRunning = false;
            }
        }

        public async Task NeverRunGetProjectsConcurrent()
        {
            if (_isUpdateRunning == true)
            {
                _logger.Log(Nos.Logger.Models.ELogLevel.Error, "Previous scheduled job is still running.", null);
            }
            else
            {
                await ProjectData();
                _isUpdateRunning = false;
            }
        }

        private async Task GetMasterClipsSequencesAndRelatives()
        {
            _isFullRunning = true;

            _logger.Log(Nos.Logger.Models.ELogLevel.Info, "Start");


            _database.Truncate();

            InterplayAssetService currentAssetService = _hlvInterplayService;

            List<AssetDescriptionType> masterClips = await GetMasterclips(currentAssetService, "");
            
            List<AssetDescriptionType> sequencesNieuws = await GetSequences(currentAssetService, "Projects/Nieuws");
            List<AssetDescriptionType> sequencesSport = await GetSequences(currentAssetService, "Projects/Sport");
            List<AssetDescriptionType> sequencesEvenementen = await GetSequences(currentAssetService, "Projects/Evenementen");
            List<AssetDescriptionType> sequencesNieuwsuur = await GetSequences(currentAssetService, "Projects/Nieuwsuur");
            List<AssetDescriptionType> sequencesEvenementenGeheim = await GetSequences(currentAssetService, "Projects/EvenementenGeheim");

            List<AssetDescriptionType> allSequences = sequencesNieuws.Concat(sequencesSport).Concat(sequencesEvenementen).Concat(sequencesNieuwsuur).Concat(sequencesEvenementenGeheim).ToList();

            await GetRelatives(currentAssetService, allSequences, masterClips);

            _logger.Log(Nos.Logger.Models.ELogLevel.Info, "End");

        }

        private async Task ProjectData()
        {
            //_isUpdateRunning = true;
            //List<AssetDescriptionType> projectsList = await GetProjects(_hlvInterplayService);
            //List<string> divisions = new List<string>() { "Nieuws", "Evenementen", "Sport", "Nieuwsuur", "EvenementenGeheim" };
            //List<string> projectTypes = new List<string>() { "Dagelijkse Projecten", "Gereserveerde Projecten"};


            //foreach(AssetDescriptionType project in projectsList)
            //{
            //    var path = project.Attributes.FirstOrDefault(x => x.Name == "Path").Value;
            //   List<ProjectRelative> relatives = _database.GetRelatives(path);
            //   List<ProjectRelative> masterclips = _database.GetMasterClips(path);
               
            //    // only if masterclip is not in relatives list add it as being not InUse. 
            //    foreach(ProjectRelative masterclip in masterclips)
            //    {
            //        if(relatives.FirstOrDefault(x => x.MasterclipMobID == masterclip.MasterclipMobID) == null)
            //        {
            //            masterclip.InUse = false;
            //            relatives.Add(masterclip);
            //        }
            //    }
            //   string creationDateText = project.Attributes.FirstOrDefault(x => x.Name == "Creation Date").Value;

            //   string division = divisions.FirstOrDefault(x => path.Contains(x));
            //   string projectType = projectTypes.FirstOrDefault(x => path.Contains(x));
            //   long masterclipSize = relatives.Where(y=>y.IsRelative == false).Sum(x => x.MediasSizeKB) / (1024*1024);
            //   long masterclipNotInUSeSize = relatives.Where(y=>y.IsRelative == false && y.InUse == false).Sum(x => x.MediasSizeKB) / (1024*1024);
            //   long relativeClipSize = relatives.Where(y=>y.IsRelative == true).Sum(x => x.MediasSizeKB) / (1024*1024);
            //   int projectHrs = (int) Math.Floor((masterclipSize + relativeClipSize) / 24.8);

            //    _database.InsertProjects(new Project()
            //    {
            //        Projectpath = path,
            //        Division = division,
            //        ProjectType = projectType,
            //        ProjectMasterclipSize = masterclipSize,
            //        ProjectRelativesSize = relativeClipSize,
            //        ProjectHrs = projectHrs,
            //        ProjectMasterclipNotInUseSize = masterclipNotInUSeSize,
            //        CreationDate = Convert.ToDateTime(creationDateText)
            //    });
            //};
        }


        //private async Task<List<AssetDescriptionType>> GetProjectFolders(InterplayAssetService assetService, string interplayPath)
        //{
        //    SearchGroupType projectFoldersSearch = assetService.ProjectFoldersSearchQuery();
        //    AssetDescriptionType[] folders = await assetService.ExcecuteSearchAsync(interplayPath, projectFoldersSearch);

        //    return only project folders having a story ID.
        //    return folders.Where(f => f.Attributes.Any(a => a.Name == "iNOS_Story_ID" && a.Value != "")).ToList();
        //}


        private async Task<List<AssetDescriptionType>> GetSequences(InterplayAssetService assetService, string interplayPath)
        {
            SearchGroupType sequenceSearch = assetService.SequenceSearchQuery();
            List<AssetDescriptionType> sequences = await assetService.ExcecuteSearchAsync(interplayPath, sequenceSearch);

            foreach (AssetDescriptionType sequenceClipDescription in sequences)
            {
                string creationDateStr = sequenceClipDescription.Attributes.FirstOrDefault(x => x.Name == "Creation Date")?.Value;
                string modifyDateStr = sequenceClipDescription.Attributes.FirstOrDefault(x => x.Name == "Modified Date")?.Value;

                Sequence sequence = new Sequence()
                {
                    MobID = sequenceClipDescription.Attributes.FirstOrDefault(x => x.Name == "MOB ID").Value,
                    AssetName = sequenceClipDescription.Attributes.FirstOrDefault(x => x.Name == "Display Name")?.Value ?? sequenceClipDescription.Attributes.FirstOrDefault(x => x.Name == "MOB ID").Value,
                    InterplayPath = Path.GetDirectoryName(sequenceClipDescription.Attributes.FirstOrDefault(x => x.Name == "Path").Value).Replace(@"\", "/"),
                    VideoID = sequenceClipDescription.Attributes.FirstOrDefault(x => x.Name == "Video ID")?.Value,
                    Duration = sequenceClipDescription.Attributes.FirstOrDefault(x => x.Name == "Duration")?.Value,
                    Tracks = sequenceClipDescription.Attributes.FirstOrDefault(x => x.Name == "Tracks")?.Value,
                    MediaFileFormat = sequenceClipDescription.Attributes.FirstOrDefault(x => x.Name == "Media File Format")?.Value,
                    MediaSize = Convert.ToInt64(sequenceClipDescription.Attributes.FirstOrDefault(x => x.Name == "Media Size")?.Value),
                    MediaStatus = sequenceClipDescription.Attributes.FirstOrDefault(x => x.Name == "Media Status")?.Value,
                    CreationDate = Helpers.TextToDateTime(creationDateStr),
                    ModifyDate = Helpers.TextToDateTime(modifyDateStr)
                };
                _database.InsertSequence(sequence);
            }

            _logger.Log(Nos.Logger.Models.ELogLevel.Info, $"seqeunces retrieved from {interplayPath} count: {sequences.Count}");
            return sequences;
        }



        private async Task<List<AssetDescriptionType>> GetMasterclips(InterplayAssetService assetService, string interplayPath)
        {
            SearchGroupType inUseMasterclipSearch = assetService.MasterclipSearchQuery(EMasterClipInUse.InUse);
            List<AssetDescriptionType> inUsemasterclips = await assetService.ExcecuteSearchAsync(interplayPath, inUseMasterclipSearch);

            SearchGroupType notInUseMasterclipSearch = assetService.MasterclipSearchQuery(EMasterClipInUse.NotInUse);
            List<AssetDescriptionType> notInUsemasterclips = await assetService.ExcecuteSearchAsync(interplayPath, notInUseMasterclipSearch);



            foreach (AssetDescriptionType masterClipDescription in inUsemasterclips)
            {
                MasterClip masterclip = MakeMasterClip(masterClipDescription);
                masterclip.InUse = EMasterClipInUse.InUse;
                _database.InsertMasterclip(masterclip);
            }

            foreach (AssetDescriptionType masterClipDescription in notInUsemasterclips)
            {
                MasterClip masterclip = MakeMasterClip(masterClipDescription);
                masterclip.InUse = EMasterClipInUse.NotInUse;
                _database.InsertMasterclip(masterclip);
            }

            _logger.Log(Nos.Logger.Models.ELogLevel.Info, $"masterclips retrieved count: {inUsemasterclips.Count + notInUsemasterclips.Count}");

            return inUsemasterclips.Concat(notInUsemasterclips).ToList();
        }

        private MasterClip MakeMasterClip(AssetDescriptionType masterClipDescription)
        {
            string creationDateStr = masterClipDescription.Attributes.FirstOrDefault(x => x.Name == "Creation Date")?.Value;
            string modifyDateStr = masterClipDescription.Attributes.FirstOrDefault(x => x.Name == "Modified Date")?.Value;

            return new MasterClip()
            {
                MobID = masterClipDescription.Attributes.FirstOrDefault(x => x.Name == "MOB ID").Value,
                AssetName = masterClipDescription.Attributes.FirstOrDefault(x => x.Name == "Display Name")?.Value ?? masterClipDescription.Attributes.FirstOrDefault(x => x.Name == "MOB ID").Value,
                InterplayPath = Path.GetDirectoryName(masterClipDescription.Attributes.FirstOrDefault(x => x.Name == "Path").Value).Replace(@"\", "/"),
                VideoID = masterClipDescription.Attributes.FirstOrDefault(x => x.Name == "Video ID")?.Value,
                Duration = masterClipDescription.Attributes.FirstOrDefault(x => x.Name == "Duration")?.Value,
                Tracks = masterClipDescription.Attributes.FirstOrDefault(x => x.Name == "Tracks")?.Value,
                MediaFileFormat = masterClipDescription.Attributes.FirstOrDefault(x => x.Name == "Media File Format")?.Value,
                MediaSize = Convert.ToInt64(masterClipDescription.Attributes.FirstOrDefault(x => x.Name == "Media Size")?.Value),
                MediaStatus = masterClipDescription.Attributes.FirstOrDefault(x => x.Name == "Media Status")?.Value,
                Workspace = masterClipDescription.Attributes.FirstOrDefault(x => x.Name == "Workspace")?.Value,
                Type = masterClipDescription.Attributes.FirstOrDefault(x => x.Name == "Type")?.Value,
                CreationDate = Helpers.TextToDateTime(creationDateStr),
                ModifyDate = Helpers.TextToDateTime(modifyDateStr)
            };
        }






        private async Task<bool> GetRelatives(InterplayAssetService assetService, List<AssetDescriptionType> sequences, List<AssetDescriptionType> masterClips)
        {
            List<string> masterMobIDs = masterClips.Select(x => x.Attributes.FirstOrDefault(a => a.Name == "MOB ID").Value).ToList();
            long count = 0;
            foreach (AssetDescriptionType sequence in sequences)
            {
                List<AssetDescriptionType> allRelatives = ( await assetService.FindRelatives(sequence.InterplayURI)).ToList();

                // first remove all offline relatives
                allRelatives.RemoveAll(x => x.Attributes.FirstOrDefault(y => y.Name == "Media Status").Value == "offline");

                // audio only masterclips and audio renders make up the bulk of all relatives but are a very small percentage of the total size. So we can affort to not include them.
                AssetDescriptionType[] masterclipRelatives = allRelatives.Where(x => ((x.Attributes.FirstOrDefault(y => y.Name == "Type").Value == "masterclip") && (x.Attributes.FirstOrDefault(y => y.Name == "Tracks")?.Value?.Contains("V") ?? true))
                                                                                  || ((x.Attributes.FirstOrDefault(y => y.Name == "Type").Value == "renderedeffect") && (x.Attributes.FirstOrDefault(y => y.Name == "Tracks")?.Value?.Contains("V") ?? true))
                                                                                  ).ToArray();
                count += masterclipRelatives.Count();

                foreach(AssetDescriptionType masterclipRelative in masterclipRelatives)
                {
                    string creationDateStr = masterclipRelative.Attributes.FirstOrDefault(x => x.Name == "Creation Date")?.Value;
                    string modifyDateStr = masterclipRelative.Attributes.FirstOrDefault(x => x.Name == "Modified Date")?.Value;
                    bool referenced = !masterMobIDs.Any(x => x == masterclipRelative.Attributes.First(z => z.Name == "MOB ID").Value);

                    Relative relative = new Relative()
                    {
                        SequenceMobId = sequence.Attributes.FirstOrDefault(x => x.Name == "MOB ID").Value,
                        AssetName = masterclipRelative.Attributes.FirstOrDefault(x => x.Name == "Display Name")?.Value ?? masterclipRelative.Attributes.FirstOrDefault(x => x.Name == "MOB ID").Value,
                        MasterClipMobId = masterclipRelative.Attributes.FirstOrDefault(x => x.Name == "MOB ID").Value,
                        InterplayPath = Path.GetDirectoryName(masterclipRelative.Attributes.FirstOrDefault(x => x.Name == "Path")?.Value)?.Replace(@"\", "/"),
                        VideoID = masterclipRelative.Attributes.FirstOrDefault(x => x.Name == "Video ID")?.Value,
                        Duration = masterclipRelative.Attributes.FirstOrDefault(x => x.Name == "Duration")?.Value,
                        Tracks = masterclipRelative.Attributes.FirstOrDefault(x => x.Name == "Tracks")?.Value,
                        MediaSize = Convert.ToInt64(masterclipRelative.Attributes.FirstOrDefault(x => x.Name == "Media Size")?.Value),
                        MediaStatus = masterclipRelative.Attributes.FirstOrDefault(x => x.Name == "Media Status")?.Value,
                        Workspace = masterclipRelative.Attributes.FirstOrDefault(x => x.Name == "Workspace")?.Value,
                        CreationDate = Helpers.TextToDateTime(creationDateStr),
                        ModifyDate = Helpers.TextToDateTime(modifyDateStr),
                        ReferencedAsset = referenced,
                        Type = masterclipRelative.Attributes.FirstOrDefault(x => x.Name == "Type")?.Value,
                    };
                    _database.InsertRelative(relative);
                }
            }
            _logger.Log(Nos.Logger.Models.ELogLevel.Info, $"relatives retrieved count: {count}");
            return true;
        }

        //private async Task<List<AssetDescriptionType>> GetProjects(InterplayAssetService assetService)
        //{
        //    List<AssetDescriptionType> allProjectsFolders = new List<AssetDescriptionType>();

        //    List<string> mainProjectFolders = _database.GetMainFolders();
        //    foreach(string folder in mainProjectFolders)
        //    {
        //        List<AssetDescriptionType> projectFolders = await GetProjectFolders(assetService, folder);

        //        allProjectsFolders.AddRange(projectFolders);
        //    }

        //    return allProjectsFolders;
        //}
    }
}
