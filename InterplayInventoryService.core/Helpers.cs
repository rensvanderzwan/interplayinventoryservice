﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace InterplayInventoryService.core
{
    public class Helpers
    {

        static public double TCtoFrames(string Timecode, int FrameRate = 25)
        {
            Regex regex = new Regex(@"(\d\d):(\d\d):(\d\d):(\d\d)");
            Match match = regex.Match(Timecode ?? "0");
            if (match.Success)
            {
                int Hours = Convert.ToInt32(match.Groups[1].Value) * 60 * 60 * FrameRate;
                int Minutes = Convert.ToInt32(match.Groups[2].Value) * 60 * FrameRate;
                int seconds = Convert.ToInt32(match.Groups[3].Value) * FrameRate;
                int Frames = Convert.ToInt32(match.Groups[4].Value);
                return Hours + Minutes + seconds + Frames;
            }
            return 0;
        }

        static public string FramesToTC(double TotalFrames, int Framerate = 25)
        {
            double Hours = Math.Floor(TotalFrames / (60 * 60 * Framerate));
            double Framesleft = TotalFrames - (Hours * Framerate * 60 * 60);
            double Minutes = Math.Floor(Framesleft / (Framerate * 60));
            Framesleft -= Minutes * Framerate * 60;
            double Seconds = Math.Floor(Framesleft / Framerate);
            Framesleft -= Seconds * Framerate;
            int Frames = (int)Math.Floor(Framesleft);
            return $"{Hours.ToString().PadLeft(2, '0')}:{Minutes.ToString().PadLeft(2, '0')}:{Seconds.ToString().PadLeft(2, '0')}:{Frames.ToString().PadLeft(2, '0')}";
        }

        static public DateTime TextToDateTime(string textDateTime)
        {
            DateTime epoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
            if(DateTime.TryParse(textDateTime, out DateTime realDateTime))
            {
                return realDateTime;
            }
            else
            {
                return epoch;
            }  
        }
    }
}
