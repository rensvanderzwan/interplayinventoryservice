﻿using InterplayInventoryService.domain;
using MySql.Data.MySqlClient;
using Nos.Logger.Contracts;
using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;

namespace InterplayInventoryService.core
{
    public class DatabaseIO
    {
        private string _myConnString = $"server=vs-hedits3.edit.nos.nl;uid=inventory_svc;pwd=webservice;database=interplay_inventory;useAffectedRows=true;SslMode=None;AllowPublicKeyRetrieval=true";
        private MySqlConnection _connection;
        private readonly INosLogger _logger;

        public DatabaseIO(INosLogger logger)
        {
            _logger = logger;
        }

        public bool Open()
        {
            if (_connection != null && _connection.State == System.Data.ConnectionState.Open) return true;

            try
            {
                _connection = new MySqlConnection(_myConnString);
                _connection.Open();
                return true;
            }
            catch (MySqlException ex)
            {
                _logger.Log(Nos.Logger.Models.ELogLevel.Error, $"Open Mysql connection1: {ex.Message}");
                return false;
            }
            catch (Exception ex)
            {
                _logger.Log(Nos.Logger.Models.ELogLevel.Error, $"Open Mysql connection2: {ex.Message}");
                return false;
            }
        }


        public void Truncate()
        {
            string queryMasterclips = @"TRUNCATE TABLE masterclips;";
            string querySequences = @"TRUNCATE TABLE sequences;";
            string queryRelatives = @"TRUNCATE TABLE relatives;";

            using (_connection)
            {
                if (Open() == false) return;

                using (var sc = new MySqlCommand(queryMasterclips, _connection))
                {
                    int Updated = sc.ExecuteNonQuery();   // number of rows affected by 
                }
                using (var sc = new MySqlCommand(querySequences, _connection))
                {
                    int Updated = sc.ExecuteNonQuery();   // number of rows affected by 
                }
                using (var sc = new MySqlCommand(queryRelatives, _connection))
                {
                    int Updated = sc.ExecuteNonQuery();   // number of rows affected by 
                }
            }
        }

        public long UpdateMasterclip(string mobID,bool inUse)
        {
            string query = @"UPDATE masterclips SET
                                in_use = COALESCE(@in_use, in_use)
                             WHERE mob_id = @mob_id";

            using (_connection)
            {
                if (Open() == false) return 0;

                using (var sc = new MySqlCommand(query, _connection))
                {
                    sc.Parameters.AddWithValue("@in_use", inUse);
                    sc.Parameters.AddWithValue("@mob_id", mobID);

                    int Updated = sc.ExecuteNonQuery();   // number of rows affected by 
                    return Updated;
                }
            }
        }

        public long InsertMasterclip(MasterClip asset)
        {
            string Query = @"INSERT INTO masterclips (
                                                mob_id,
                                                asset_name,
                                                video_id,
                                                interplay_path,
                                                duration_frames,
                                                tracks,
                                                media_size,
                                                media_file_format,
                                                media_status,
                                                creation_date,
                                                workspace,
                                                in_use,
                                                type,
                                                modify_date
                                              ) VALUES (
                                                @mob_id,
                                                @asset_name,
                                                @video_id,
                                                @interplay_path,
                                                @duration_frames,
                                                @tracks,
                                                @media_size,
                                                @media_file_format,
                                                @media_status,
                                                @creation_date,
                                                @workspace,
                                                @in_use,
                                                @type,
                                                @modify_date
                                              )";

            using (_connection)
            {
                if (Open() == false) return 0;

                using (var sc = new MySqlCommand(Query, _connection))
                {
                    try
                    {
                        sc.Parameters.AddWithValue("@mob_id", asset.MobID);
                        sc.Parameters.AddWithValue("@asset_name", asset.AssetName);
                        sc.Parameters.AddWithValue("@video_id", asset.VideoID);
                        sc.Parameters.AddWithValue("@interplay_path", asset.InterplayPath);
                        sc.Parameters.AddWithValue("@duration_frames", Helpers.TCtoFrames( asset.Duration));
                        sc.Parameters.AddWithValue("@tracks", asset.Tracks);
                        sc.Parameters.AddWithValue("@media_size", asset.MediaSize);
                        sc.Parameters.AddWithValue("@media_file_format", asset.MediaFileFormat);
                        sc.Parameters.AddWithValue("@media_status", asset.MediaStatus);
                        sc.Parameters.AddWithValue("@creation_date", asset.CreationDate);
                        sc.Parameters.AddWithValue("@modify_date", asset.ModifyDate);
                        sc.Parameters.AddWithValue("@workspace", asset.Workspace ?? "Unknown");
                        sc.Parameters.AddWithValue("@in_use", asset.InUse);
                        sc.Parameters.AddWithValue("@type", asset.Type);

                        if (sc.ExecuteNonQuery() == 1)      // 1 = new / 2 = update / 0 = not changed
                        {
                        }
                        return sc.LastInsertedId;
                    }
                    catch (MySqlException ex)
                    {
                        if (ex.Number == 1062)
                        {

                        }
                        else
                        {
                            string ErrorMessage = $"Message: {ex.Message} InnerException: {ex.InnerException}";
                            _logger.Log(Nos.Logger.Models.ELogLevel.Error, "InsertMasterclip: " + ErrorMessage);
                        }
                        return 0;
                    }
                }
            }
        }


        public long InsertSequence(Sequence sequence)
        {
            string Query = @"INSERT INTO sequences (
                                                mob_id,
                                                asset_name,
                                                video_id,
                                                interplay_path,
                                                duration_frames,
                                                tracks,
                                                creation_date,
                                                modify_date
                                              ) VALUES (
                                                @mob_id,
                                                @asset_name,
                                                @video_id,
                                                @interplay_path,
                                                @duration_frames,
                                                @tracks,
                                                @creation_date,
                                                @modify_date
                                              )";

            using (_connection)
            {
                if (Open() == false) return 0;

                using (var sc = new MySqlCommand(Query, _connection))
                {
                    try
                    {
                        sc.Parameters.AddWithValue("@mob_id", sequence.MobID);
                        sc.Parameters.AddWithValue("@asset_name", sequence.AssetName);
                        sc.Parameters.AddWithValue("@video_id", sequence.VideoID);
                        sc.Parameters.AddWithValue("@interplay_path", sequence.InterplayPath);
                        sc.Parameters.AddWithValue("@duration_frames", Helpers.TCtoFrames(sequence.Duration));
                        sc.Parameters.AddWithValue("@tracks", sequence.Tracks);
                        sc.Parameters.AddWithValue("@creation_date", sequence.CreationDate);
                        sc.Parameters.AddWithValue("@modify_date", sequence.ModifyDate);

                        if (sc.ExecuteNonQuery() == 1)      // 1 = new / 2 = update / 0 = not changed
                        {
                        }
                        return sc.LastInsertedId;
                    }
                    catch (MySqlException ex)
                    {
                        if (ex.Number == 1062)
                        {

                        }
                        else
                        {
                            string ErrorMessage = $"Message: {ex.Message} InnerException: {ex.InnerException}";
                            _logger.Log(Nos.Logger.Models.ELogLevel.Error, "InsertSequence: " + ErrorMessage);
                        }
                        return 0;
                    }
                }
            }
        }


        public long InsertRelative(Relative relative)
        {
            string Query = @"INSERT INTO relatives (
                                                sequence_mob_id,
                                                master_clip_mob_id,
                                                asset_name,
                                                video_id,
                                                interplay_path,
                                                duration_frames,
                                                tracks,
                                                creation_date,
                                                modify_date,
                                                media_status,
                                                referenced_asset,
                                                workspace,
                                                type,
                                                media_size
                                              ) VALUES (
                                                @sequence_mob_id,
                                                @master_clip_mob_id,
                                                @asset_name,
                                                @video_id,
                                                @interplay_path,
                                                @duration_frames,
                                                @tracks,
                                                @creation_date,
                                                @modify_date,
                                                @media_status,
                                                @referenced_asset,
                                                @workspace,
                                                @type,
                                                @media_size
                                              )";

            using (_connection)
            {
                if (Open() == false) return 0;

                using (var sc = new MySqlCommand(Query, _connection))
                {
                    try
                    {
                        sc.Parameters.AddWithValue("@sequence_mob_id", relative.SequenceMobId);
                        sc.Parameters.AddWithValue("@master_clip_mob_id", relative.MasterClipMobId);
                        sc.Parameters.AddWithValue("@asset_name", relative.AssetName);
                        sc.Parameters.AddWithValue("@video_id", relative.VideoID);
                        sc.Parameters.AddWithValue("@interplay_path", relative.InterplayPath);
                        sc.Parameters.AddWithValue("@duration_frames", Helpers.TCtoFrames(relative.Duration));
                        sc.Parameters.AddWithValue("@tracks", relative.Tracks);
                        sc.Parameters.AddWithValue("@creation_date", relative.CreationDate);
                        sc.Parameters.AddWithValue("@modify_date", relative.ModifyDate);
                        sc.Parameters.AddWithValue("@media_status", relative.MediaStatus);
                        sc.Parameters.AddWithValue("@referenced_asset", relative.ReferencedAsset);
                        sc.Parameters.AddWithValue("@media_size", relative.MediaSize);
                        sc.Parameters.AddWithValue("@workspace", relative.Workspace);
                        sc.Parameters.AddWithValue("@type", relative.Type);

                        if (sc.ExecuteNonQuery() == 1)      // 1 = new / 2 = update / 0 = not changed
                        {
                        }
                        return sc.LastInsertedId;
                    }
                    catch (MySqlException ex)
                    {
                        if (ex.Number == 1062)
                        {

                        }
                        else
                        {
                            string ErrorMessage = $"Message: {ex.Message} InnerException: {ex.InnerException}";
                            _logger.Log(Nos.Logger.Models.ELogLevel.Error, "InsertRelative: " + ErrorMessage);
                        }
                        return 0;
                    }
                }
            }
        }



        public List<string> GetMainFolders()
        {
            List<string> folders = new List<string>();

            string query = @"SELECT
 	                             * 
                             FROM
	                             main_folders
                             WHERE
	                             type = @type";

            using (_connection)
            {
                if (Open() == false) return null;
                using (var sc = new MySqlCommand(query, _connection))
                {
                    sc.Parameters.AddWithValue("@type", "Projects");
                    using (MySqlDataReader mySQlReader = sc.ExecuteReader())
                    {
                        while (mySQlReader.Read())
                        {
                            folders.Add(mySQlReader.GetString("path"));
                        };
                        return folders;
                    };
                }
            }
        }

        public bool InsertProjects(Project project)
        {
            string query = @"INSERT INTO Projects (
                                                    project_path,
                                                    division,
                                                    project_type,
                                                    project_masterclip_size,
                                                    project_masterclip_not_in_use_size,
                                                    creation_date,
                                                    project_relatives_size
                                                   ) VALUES (
                                                    @project_path,
                                                    @division,
                                                    @project_type,
                                                    @project_masterclip_size,
                                                    @project_masterclip_not_in_use_size,
                                                    @creation_date,
                                                    @project_relatives_size
                                                   )";

            using (_connection)
            {
                if (Open() == false) return false;

                using (var sc = new MySqlCommand(query, _connection))
                {
                    try
                    {
                        sc.Parameters.AddWithValue("@project_path", project.Projectpath);
                        sc.Parameters.AddWithValue("@division", project.Division);
                        sc.Parameters.AddWithValue("@project_type", project.ProjectType);
                        sc.Parameters.AddWithValue("@project_masterclip_size", project.ProjectMasterclipSize);
                        sc.Parameters.AddWithValue("@project_masterclip_not_in_use_size", project.ProjectMasterclipNotInUseSize);
                        sc.Parameters.AddWithValue("@project_relatives_size", project.ProjectRelativesSize);
                        sc.Parameters.AddWithValue("@creation_date", project.CreationDate);

                        if (sc.ExecuteNonQuery() == 1)      // 1 = new / 2 = update / 0 = not changed
                        {
                        }
                        return true;
                    }
                    catch (MySqlException ex)
                    {
                        if (ex.Number == 1062)
                        {

                        }
                        else
                        {
                            string ErrorMessage = $"Message: {ex.Message} InnerException: {ex.InnerException}";
                            _logger.Log(Nos.Logger.Models.ELogLevel.Error, "InsertRelative: " + ErrorMessage);
                        }
                        return false;
                    }
                }
            }

        }

        public List<ProjectRelative> GetMasterClips(string interplaypath)
        {
            List<ProjectRelative> masterClipList = new List<ProjectRelative>();
            string query = @"SELECT 
                                * 
                             FROM 
                                masterclips 
                             WHERE
                                interplay_path LIKE @interplay_path";

            using (_connection)
            {
                List<ProjectRelative> projectRelativeList = new List<ProjectRelative>();
                if (Open() == false) return null;
                using (var sc = new MySqlCommand(query, _connection))
                {
                    sc.Parameters.AddWithValue("@interplay_path", interplaypath + '%');
                    using (MySqlDataReader mySQlReader = sc.ExecuteReader())
                    {
                        while (mySQlReader.Read())
                        {
                            ProjectRelative masterclip = new ProjectRelative();
                            masterclip.AssetName = mySQlReader.GetString("asset_name");
                            masterclip.DurationInFrames = Convert.ToInt64(mySQlReader["duration_frames"]);
                            masterclip.IsRelative = false;
                            masterclip.MediasSizeKB = Convert.ToInt64(mySQlReader["media_size"]);
                            if (!mySQlReader.IsDBNull(mySQlReader.GetOrdinal("media_status")))
                                masterclip.MediasStatus = mySQlReader.GetString("media_status");
                            if (!mySQlReader.IsDBNull(mySQlReader.GetOrdinal("workspace")))
                                masterclip.Workspace = mySQlReader.GetString("workspace");
                            if (!mySQlReader.IsDBNull(mySQlReader.GetOrdinal("tracks")))
                                masterclip.Tracks = mySQlReader.GetString("tracks");
                            masterclip.MasterclipMobID = mySQlReader.GetString("mob_id");

                            masterClipList.Add(masterclip);
                        }
                        return masterClipList;
                    }
                }
            }
        }


            public List<ProjectRelative> GetRelatives(string interplaypath)
        {
            string query = @"SELECT
	                             relatives.asset_name AS RelativeName,
	                             if( masterclips.asset_name is null,  1, 0) as IsRelative,
                                 relatives.duration_frames AS durationFrames,
	                             relatives.tracks AS tracks,
	                             relatives.media_size mediaSize,
	                             relatives.media_status AS mediaStatus,
	                             relatives.workspace AS Workspace,
	                             COUNT( relatives.master_clip_mob_id ) AS occurences,
	                             GROUP_CONCAT( sequences.asset_name, ' ' ) AS sequenceList,
                                 master_clip_mob_id,
                                 sequence_mob_id
                             FROM
	                             sequences
	                             LEFT JOIN relatives ON(sequences.mob_id = relatives.sequence_mob_id)
                                 LEFT JOIN masterclips ON(relatives.master_clip_mob_id = masterclips.mob_id)
                             WHERE
                                 sequences.interplay_path LIKE @interplay_path
                                 AND relatives.media_status = 'online'
                             GROUP BY
                                 relatives.master_clip_mob_id; ";

            using (_connection)
            {
                List<ProjectRelative> projectRelativeList = new List<ProjectRelative>();
                if (Open() == false) return null;
                using (var sc = new MySqlCommand(query, _connection))
                {
                    try
                    {
                        sc.Parameters.AddWithValue("@interplay_path", interplaypath + '%');
                        using (MySqlDataReader mySQlReader = sc.ExecuteReader())
                        {
                            while (mySQlReader.Read())
                            {
                                ProjectRelative relative = new ProjectRelative();
                                if (!mySQlReader.IsDBNull(mySQlReader.GetOrdinal("RelativeName")))
                                    relative.AssetName = mySQlReader.GetString("RelativeName");
                                relative.IsRelative = Convert.ToBoolean(mySQlReader["IsRelative"]);
                                relative.InUse = true;
                                if (!mySQlReader.IsDBNull(mySQlReader.GetOrdinal("mediaStatus")))
                                    relative.MediasStatus = mySQlReader.GetString("mediaStatus");
                                relative.DurationInFrames = Convert.ToInt64(mySQlReader["durationFrames"]);
                                relative.MediasSizeKB = Convert.ToInt64(mySQlReader["mediaSize"]);
                                if (!mySQlReader.IsDBNull(mySQlReader.GetOrdinal("Workspace")))
                                    relative.MediasStatus = mySQlReader.GetString("Workspace");
                                relative.Ocurrence = Convert.ToInt32(mySQlReader["occurences"]);
                                string SeqeunceList = mySQlReader.GetString("sequenceList");
                                relative.Sequences = SeqeunceList.Split(',');
                                if (!mySQlReader.IsDBNull(mySQlReader.GetOrdinal("Workspace")))
                                    relative.Workspace = mySQlReader.GetString("Workspace");
                                if (!mySQlReader.IsDBNull(mySQlReader.GetOrdinal("tracks")))
                                    relative.Tracks = mySQlReader.GetString("tracks");
                                relative.MasterclipMobID = mySQlReader.GetString("master_clip_mob_id");
                                relative.SequenceMobID = mySQlReader.GetString("sequence_mob_id");
                                projectRelativeList.Add(relative);
                            };
                            return projectRelativeList;
                        }
            
                    }
                    catch(Exception ex)
                    {
                        _logger.Log(Nos.Logger.Models.ELogLevel.Error, "GetRelatives: " + ex.ToString());
                        return projectRelativeList;
                    }
                }
            }
        }



    }
}
